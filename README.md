# NEWS APP
News Information App loaded from News.org API 

## Installation

Open terminal and change directory to your desired folder, then:

```bash
git clone https://gitlab.com/naofalhakim/react-native-news.git
cd React-Native-News
yarn install
cd ios
pod install
```

## Run Your App

```bash
npx react-native run-android
npx react-native run-ios
```
