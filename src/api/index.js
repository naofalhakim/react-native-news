import axios from 'axios';
const API_KEY = '31b410c9455747fbb16ce13980e9a25f'; // 'ba7b3b8474ed40d2b376e0060a8cfceb',

const GET_NEWS_LIST = '/top-headlines';

const getNews = (
  params = {country: 'au', pageSize: 10, page: 1},
  onPre,
  onSuccess,
  onError,
) => {
  let key = {
    ID: 12345,
    apiKey: API_KEY,
  };

  onPre();

  axios
    .get(GET_NEWS_LIST, {
      params: {...params, ...key},
      baseURL: 'https://newsapi.org/v2',
      headers: {'Access-Control-Allow-Origin': '*'},
      Accept: 'application/json',
    })
    .then(function (response) {
      onSuccess(response);
    })
    .catch(function (error) {
      onError(error.message);
    });
};

export {getNews};
