import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {color, dimens, fonts} from '../../utils';

const ButtonMenu = ({text, isActive, onPress}) => {
  return (
    <TouchableOpacity
      //   onPress={() => this.onSelectedMenu(item.id)}
      onPress={onPress}
      style={isActive ? styles.menuItemActive : styles.menuItem}>
      <Text style={isActive ? styles.itemTextActive : styles.itemText}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

export default ButtonMenu;
const styles = StyleSheet.create({
  menuItemActive: {
    borderRadius: dimens.small,
    backgroundColor: color.primary,
    paddingHorizontal: dimens.default_12,
    paddingVertical: dimens.small_6,
    marginHorizontal: dimens.small_5,
    height: 40,
    alignItems: 'center',
  },
  menuItem: {
    borderColor: color.primary,
    borderWidth: 1,
    paddingHorizontal: dimens.default_12,
    paddingVertical: dimens.small_6,
    color: color.black,
    marginHorizontal: dimens.small_5,
    borderRadius: dimens.small,
    maxHeight: 40,
    alignItems: 'center',
  },

  itemText: {
    fontFamily: fonts.plusjakarta_regular,
    fontSize: dimens.default_14,
    lineHeight: dimens.default_21,
    color: color.primary,
  },
  itemTextActive: {
    fontFamily: fonts.plusjakarta_regular,
    fontSize: dimens.default_14,
    lineHeight: dimens.default_21,
    color: color.white,
  },
});
