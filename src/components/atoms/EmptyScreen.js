import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {IcNews} from '../../assets/icons';
import {color, dimens, fonts} from '../../utils';

export default function EmptyScreen() {
  return (
    <View style={styles.container}>
      <Image style={styles.img} source={IcNews} />
      <View style={styles.textOuter}>
        <Text style={styles.text}>No News Available</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
  },
  img: {
    width: dimens.large_80,
    height: dimens.large_80,
    marginBottom: dimens.default,
  },
  textOuter: {
    backgroundColor: color.primary,
    padding: dimens.small,
    borderRadius: dimens.small_5,
  },
  text: {
    fontFamily: fonts.plusjakarta_bold,
    color: color.white,
    fontSize: dimens.default_14,
  },
});
