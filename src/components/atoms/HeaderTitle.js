import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {IcBack} from '../../assets/icons';
import {color, dimens, fonts} from '../../utils';

const HeaderTitle = ({title = '', backButton = false, navigation}) => {
  const backAction = () => {
    navigation.goBack();
  };
  return (
    <View style={styles.container}>
      <View style={styles.horizontalLayout}>
        {backButton && (
          <TouchableOpacity onPress={backAction}>
            <Image style={styles.backButton} source={IcBack} />
          </TouchableOpacity>
        )}
        <Text style={styles.headerText}>{title}</Text>
      </View>
    </View>
  );
};

export default HeaderTitle;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: dimens.default,
  },
  backButton: {
    marginRight: dimens.small_6,
    width: dimens.default,
    height: dimens.default,
  },
  horizontalLayout: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: fonts.plusjakarta_bold,
    fontSize: dimens.medium,
    lineHeight: dimens.large_36,
    color: color.black,
  },
});
