import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {fonts, color, dimens} from '../../utils';

const fontFamily = type => {
  switch (type) {
    case 'bold':
      return fonts.plusjakarta_bold;
    default:
      return fonts.plusjakarta_medium;
    case 'regular':
      return fonts.plusjakarta_regular;
  }
};

export default function TextContent({
  children,
  type,
  lineHeight = dimens.default_21,
  size = dimens.default_14,
  textColor = color.black_1,
  styles = {},
}) {
  return (
    <Text
      style={[
        {
          fontFamily: fontFamily(type),
          fontSize: size,
          lineHeight: lineHeight,
          color: textColor,
        },
        styles,
      ]}>
      {children || ''}
    </Text>
  );
}
