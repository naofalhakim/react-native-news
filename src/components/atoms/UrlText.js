import React from 'react';
import {Linking, StyleSheet, Text} from 'react-native';
import {color, dimens, fonts} from '../../utils';

export default function UrlText({url}) {
  return (
    <Text onPress={() => Linking.openURL(url)} style={styles.urlText}>
      Click for more information
      {url ? '' : ' (not supported)'}
    </Text>
  );
}

const styles = StyleSheet.create({
  urlText: {
    fontFamily: fonts.plusjakarta_bold,
    fontSize: dimens.default_12,
    lineHeight: dimens.default_21,
    color: color.purple,
    marginTop: dimens.large_120,
  },
});
