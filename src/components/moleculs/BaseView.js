import React, {useEffect, useState} from 'react';
import {StyleSheet, View, StatusBar, SafeAreaView, Alert} from 'react-native';
import {HeaderTitle} from '../atoms';
import {color, dimens} from '../../utils';
import NetInfo, {useNetInfo} from '@react-native-community/netinfo';

const BaseView = ({
  navigation = null,
  title = '',
  children,
  backButton,
  noHeader = false,
}) => {
  // const netInfo = useNetInfo(); // declare the constant
  const [isOffline, setIsOffline] = useState(false);

  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener(state => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setIsOffline(offline);
    });

    return () => removeNetInfoSubscription();
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar animated={true} backgroundColor={color.white} />
      {isOffline &&
        Alert.alert(`Your connection is ${isOffline ? 'bad' : 'good'}`)}
      <View style={styles.base}>
        {!noHeader && (
          <HeaderTitle
            title={title}
            backButton={backButton}
            navigation={navigation}
          />
        )}
        {children}
      </View>
    </SafeAreaView>
  );
};
export default BaseView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  containerNoInternet: {
    flex: 1,
    backgroundColor: color.green,
  },
  base: {
    padding: dimens.default,
    flex: 1,
  },
});
