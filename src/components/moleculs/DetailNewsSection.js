import React from 'react';
import {StyleSheet, Image, ScrollView} from 'react-native';
import {TextContent, UrlText} from '..';
import {color, dimens, FORMAT_DATE_1, toDateTimeFormat} from '../../utils';

const DetailNewsSection = ({news}) => {
  return (
    <ScrollView>
      <Image
        style={styles.bigThumbnail}
        source={news.urlToImage ? {uri: news.urlToImage} : null}
      />
      <TextContent
        type="bold"
        size={dimens.default_12}
        lineHeight={dimens.default_18}
        textColor={color.gray_3}
        styles={styles.sourceNewsText}>
        {news.source ? news.source.name : ''}
      </TextContent>

      <TextContent
        type="bold"
        size={dimens.default_18}
        lineHeight={dimens.default_28}
        textColor={color.black}>
        {news.title}
      </TextContent>

      <TextContent
        size={dimens.default_12}
        lineHeight={dimens.default_18}
        styles={styles.dateText}>
        {`${news.author ? news.author + ' -' : ''} ${
          news.publishedAt
            ? toDateTimeFormat(news.publishedAt, FORMAT_DATE_1)
            : ''
        }`}
      </TextContent>
      <TextContent styles={styles.contentText} textColor={color.gray_2}>
        {news.description}
      </TextContent>
      <TextContent styles={styles.contentText} textColor={color.gray_2}>
        {news.content}
      </TextContent>
      <UrlText url={news.url} />
    </ScrollView>
  );
};

export default DetailNewsSection;

const styles = StyleSheet.create({
  bigThumbnail: {
    height: 170,
    width: 340,
    backgroundColor: color.gray,
  },
  sourceNewsText: {
    marginTop: dimens.small,
  },
  dateText: {
    marginTop: dimens.small,
  },
  contentText: {
    marginTop: dimens.medium,
  },
});
