import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import {color, dimens, fonts} from '../../utils';

const HeadlineSection = ({onPress, news}) => {
  return (
    <TouchableWithoutFeedback style={{marginTop: 10}} onPress={onPress}>
      <View>
        {news.urlToImage ? (
          <Image
            style={styles.bigThumbnail}
            source={news.urlToImage ? {uri: news.urlToImage} : null}
          />
        ) : (
          <View style={styles.bigThumbnail} />
        )}
        <Text style={styles.sourceNewsText}>
          {news.source ? news.source.name : ''}
        </Text>
        <Text style={styles.titleHeaderText}>
          {`${news.title || ''} - ${news.source ? news.source.name : ''}`}
        </Text>
        <Text style={styles.contentText}>
          {`${
            news.description ? news.description.substring(0, 100) + '...' : ''
          }`}
        </Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default HeadlineSection;

const styles = StyleSheet.create({
  bigThumbnail: {
    height: 170,
    width: 340,
    backgroundColor: color.gray,
  },
  sourceNewsText: {
    fontFamily: fonts.plusjakarta_bold,
    fontSize: dimens.default_11,
    lineHeight: dimens.default_18,
    color: color.gray_3,
  },
  titleHeaderText: {
    fontFamily: fonts.plusjakarta_bold,
    fontSize: dimens.default_14,
    lineHeight: dimens.default_21,
    color: color.black,
  },
  contentText: {
    fontFamily: fonts.plusjakarta_medium,
    fontSize: dimens.small_10,
    lineHeight: dimens.default_18,
    color: color.gray_2,
  },
});
