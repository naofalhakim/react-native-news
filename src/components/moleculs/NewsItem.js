import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  color,
  dimens,
  fonts,
  FORMAT_DATE_1,
  toDateTimeFormat,
} from '../../utils';

const NewsItem = ({onPress, item}) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.container}>
        <Image
          style={styles.smallThumbnail}
          source={item.urlToImage ? {uri: item.urlToImage} : null}
        />

        <View>
          <Text style={styles.sourceNewsText}>{item.source.name}</Text>
          <Text style={styles.titleHeaderText}>{item.title}</Text>
          <Text style={styles.itemSmallText}>
            {toDateTimeFormat(item.publishedAt, FORMAT_DATE_1)}
          </Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};
export default NewsItem;

const styles = StyleSheet.create({
  container: {flex: 1, display: 'flex', flexDirection: 'row'},
  smallThumbnail: {
    height: 81,
    width: 93,
    backgroundColor: color.gray,
    marginRight: dimens.default,
  },
  itemSmallText: {
    fontFamily: fonts.plusjakarta_regular,
    fontSize: dimens.small_10,
    lineHeight: dimens.default_14,
    color: color.gray_2,
  },
  sourceNewsText: {
    fontFamily: fonts.plusjakarta_bold,
    fontSize: dimens.default_11,
    lineHeight: dimens.default_18,
    color: color.gray_3,
  },
  titleHeaderText: {
    fontFamily: fonts.plusjakarta_bold,
    fontSize: dimens.default_14,
    lineHeight: dimens.default_21,
    color: color.black,
  },
});
