import HeadlineSection from './HeadlineSection';
import DetailNewsSection from './DetailNewsSection';
import BaseView from './BaseView';
import NewsItem from './NewsItem';

export {HeadlineSection, DetailNewsSection, BaseView, NewsItem};
