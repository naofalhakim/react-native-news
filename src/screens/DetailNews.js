import React, {Component} from 'react';
import {BaseView, DetailNewsSection} from '../components';

export default class DetailNews extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <BaseView
        navigation={this.props.navigation}
        title="Detail News"
        backButton={true}>
        <DetailNewsSection news={this.props.route.params.news} />
      </BaseView>
    );
  }
}
