import React, {Component} from 'react';
import {StyleSheet, View, FlatList, Alert} from 'react-native';
import {color, COUNTRIES, dimens} from '../utils';
import axios from 'axios';
import {
  BaseView,
  ButtonMenu,
  EmptyScreen,
  HeadlineSection,
  LoadingScreen,
  NewsItem,
} from '../components';
import {getNews} from '../api';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedMenu: 'us',
      mainNews: {},

      articles: [],
      isLoading: false,
      page: 1,
      seed: 20,
      totalCount: 0,
      refresh: false,

      error: '',
    };

    this._renderMenuItem = this._renderMenuItem.bind(this);
    this._renderNewsItem = this._renderNewsItem.bind(this);
    this._renderHeaderList = this._renderHeaderList.bind(this);
    this.onPageChange = this.onPageChange.bind(this);
  }

  componentDidMount() {
    const {selectedMenu, page, seed} = this.state;
    getNews(
      {
        country: selectedMenu,
        page,
        pageSize: seed,
      },
      this.onPre,
      response => {
        let dataNews = response.data.articles;
        this.setState({
          mainNews: dataNews.pop() || {},
          articles: dataNews,
          isLoading: false,
          totalCount: response.data.totalResults,
        });
      },
      this.onErrorResponse,
    );
  }

  _renderMenuItem({item}) {
    return (
      <ButtonMenu
        isActive={this.state.selectedMenu === item.id}
        key={item.id}
        text={item.text}
        onPress={() => this.onSelectedMenu(item.id)}
      />
    );
  }

  _renderNewsItem({item, index}) {
    return (
      <NewsItem
        onPress={() =>
          this.props.navigation.navigate('DetailNews', {
            news: item,
          })
        }
        item={item}
        key={index.toString()}
      />
    );
  }

  _renderHeaderList() {
    return this.state.articles.length ? (
      <View>
        <HeadlineSection
          news={this.state.mainNews}
          onPress={() =>
            this.props.navigation.navigate('DetailNews', {
              news: this.state.mainNews,
            })
          }
        />
        <View style={styles.verticalSpacerThin} />
      </View>
    ) : null;
  }

  onSelectedMenu(id) {
    this.setState(
      {
        selectedMenu: id,
        page: 1,
      },
      () =>
        getNews(
          {
            country: id,
            pageSize: this.state.seed,
          },
          this.onPre,
          response => {
            let dataNews = response.data.articles;

            this.setState({
              mainNews: dataNews.pop() || {},
              articles: dataNews,
              isLoading: false,
              totalCount: response.data.totalResults,
            });
          },
          this.onErrorResponse,
        ),
    );
  }

  onPageChange() {
    const {page, seed, totalCount, selectedMenu} = this.state;
    if (page * seed >= totalCount) {
      return;
    }

    this.setState(
      {
        page: this.state.page + 1,
      },
      () =>
        getNews(
          {
            country: selectedMenu,
            page: this.state.page,
            pageSize: seed,
          },
          this.onPre,
          response => {
            let dataNews = response.data.articles;
            this.setState({
              articles: [...this.state.articles, ...dataNews],
              isLoading: false,
            });
          },
          this.onErrorResponse,
        ),
    );
  }

  onErrorResponse = error => {
    this.setState(
      {
        isLoading: false,
      },
      () => Alert.alert(error),
    );
  };

  onPre = () => {
    this.setState({
      isLoading: true,
    });
  };

  getNews = (country = 'au', onSuccess, onError) => {
    this.setState({
      isLoading: true,
    });

    axios
      .get('/top-headlines', {
        params: {
          ID: 12345,
          country: country,
          pageSize: this.state.seed,
          page: this.state.page,
          //   apiKey: 'ba7b3b8474ed40d2b376e0060a8cfceb',
          apiKey: '31b410c9455747fbb16ce13980e9a25f',
        },

        baseURL: 'https://newsapi.org/v2',
        headers: {'Access-Control-Allow-Origin': '*'},
        Accept: 'application/json',
      })
      .then(function (response) {
        onSuccess(response);
      })
      .catch(function (error) {
        onError(error.message);
      });
  };

  render() {
    return (
      <BaseView navigation={this.props.navigation} title={'News'}>
        {/* //menuSection */}
        <FlatList
          style={styles.menuContainer}
          data={COUNTRIES}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={this._renderMenuItem}
          extraData={this.state.selectedMenu}
        />
        {/* //Headline */}
        <View style={styles.verticalSpacer} />

        {/* Infinite List */}
        <FlatList
          ListHeaderComponent={this._renderHeaderList}
          data={this.state.articles}
          ItemSeparatorComponent={() => (
            <View style={styles.verticalSpacerThin} />
          )}
          renderItem={this._renderNewsItem}
          refresh={this.state.isLoading}
          onEndReachedThreshold={1}
          onEndReached={this.onPageChange}
          ListEmptyComponent={<EmptyScreen />}
        />
        {this.state.articles.length > 0 && (
          <LoadingScreen isModalActive={this.state.isLoading} />
        )}
      </BaseView>
    );
  }
}

const styles = StyleSheet.create({
  verticalSpacer: {
    height: dimens.small_5,
    backgroundColor: color.gray_4,
    marginBottom: dimens.default,
  },
  verticalSpacerThin: {
    height: 1,
    backgroundColor: color.gray_4,
    marginBottom: dimens.large_25,
    marginTop: dimens.default,
  },

  menuContainer: {
    marginBottom: dimens.default,
    flexGrow: 0,
    minHeight: 45,
  },
});
