export const color = {
  primary: '#6468E5',
  primaryTransparent: '#6468E590',
  purple: '#541AB2',
  purple_2: 'rgba(115, 30, 255, 0.2)',
  purple_3: '#F9F5FF',
  purple_4: '#EFE5FF',
  black: '#333',
  black_1: '#000',
  black_2: 'rgba(0, 0, 0, 0.5)',
  white: '#fff',
  white_2: '#FAFAFA',
  white_3: '#F5F5F5',
  gray: '#DADADA',
  gray_2: '#757575',
  gray_3: '#C2C2C2',
  gray_4: '#E6E6E6',
  red: '#D13661',
  red_2: '#FEE3DC',
  red_3: '#F85026',
  berry: '#FFECF1', //0
  green: '#45AB55',
  green_2: '#E0F3E3',
};
