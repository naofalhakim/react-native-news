export const fonts = {
  plusjakarta_regular: 'PlusJakartaSans-Regular',
  plusjakarta_medium: 'PlusJakartaSans-Medium',
  plusjakarta_bold: 'PlusJakartaSans-Bold',
};
