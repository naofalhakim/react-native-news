import moment from 'moment';

export const FORMAT_TIME_1 = 'h:mm'; //2:04
export const FORMAT_TIME_2 = 'h:mm A'; //2:04 AM or PM
export const FORMAT_DATE_1 = 'MMMM DD, YYYY'; //May 21, 2021
export const FORMAT_DATE_2 = 'DD MMMM YYYY'; //13 Juli 2020

/*convert from new Date() format to time 2:04 AM*/
export const toDateTimeFormat = (dateTime, format) =>
  moment(dateTime).format(format);
