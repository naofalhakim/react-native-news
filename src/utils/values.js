export const COUNTRIES = [
  {id: 'us', text: 'United State'},
  {id: 'uk', text: 'United Kingdom'},
  {id: 'au', text: 'Australia'},
  {id: 'sg', text: 'Singapore'},
  {id: 'ae', text: 'United Arab Emirates'},
  {id: 'ar', text: 'Argentina'},
  {id: 'at', text: 'Austria'},
  {id: 'bg', text: 'Belgium'},
];

export const RESPONSE = {
  status: 'ok',
  totalResults: 38,
  articles: [
    {
      source: {
        id: 'nbc-news',
        name: 'NBC News',
      },
      author: 'Denise Chow',
      title:
        'NASA launches first mission to test asteroid deflection - NBC News',
      description:
        'NASA will crash a spacecraft into an asteroid for the first time ever as part of the Double Asteroid Redirection Test (DART), a planetary defense mission.',
      url: 'https://www.nbcnews.com/science/space/nasa-launch-first-ever-mission-test-asteroid-deflection-rcna5698',
      urlToImage:
        'https://media-cldnry.s-nbcnews.com/image/upload/t_nbcnews-fp-1200-630,f_auto,q_auto:best/rockcms/2021-11/211124-nasa-asteroid-dart-mb-0950-f4cb94.jpg',
      publishedAt: '2021-11-24T10:09:00Z',
      content:
        "The probe will spend almost a year journeying to an asteroid system more than 6.5 million miles away from Earth. The mission's target is Dimorphos, a space rock measuring 525 feet across that orbits … [+2660 chars]",
    },
    {
      source: {
        id: 'cnn',
        name: 'CNN',
      },
      author: 'Francesca Street, CNN',
      title:
        "Great Barrier Reef explodes into life in 'magical' spawning event - CNN",
      description:
        'The Great Barrier Reef has "given birth" in its annual coral spawn, creating a cacophony of color on the Australian landmark.',
      url: 'https://www.cnn.com/travel/article/great-barrier-reef-coral-spawn-scn/index.html',
      urlToImage:
        'https://cdn.cnn.com/cnnnext/dam/assets/211123164201-20211123-flynn-reef-point-break-acropora-releasingcredit-gareth-phillips-reef-teach135-super-tease.jpg',
      publishedAt: '2021-11-24T07:37:03Z',
      content:
        '(CNN) The Great Barrier Reef has "given birth" in its annual coral spawn, creating a cacophony of color on the Australian landmark.\r\nScientists working beneath the waves say they witnessed the event,… [+3002 chars]',
    },
    {
      source: {
        id: null,
        name: 'ESPN',
      },
      author: 'Myron Medcalf',
      title:
        "No. 1 Gonzaga dominates No. 2 UCLA in men's basketball showdown - ESPN",
      description:
        'No. 1 Gonzaga dominated No. 2 UCLA in an 83-63 victory Tuesday night that turned an anticipated showdown into a showcase for the Bulldogs.',
      url: 'https://www.espn.com/mens-college-basketball/story/_/id/32703594/no-1-gonzaga-dominates-no-2-ucla-marquee-men-basketball-matchup',
      urlToImage:
        'https://a2.espncdn.com/combiner/i?img=%2Fphoto%2F2021%2F1124%2Fr941625_1296x729_16%2D9.jpg',
      publishedAt: '2021-11-24T07:07:41Z',
      content:
        "LAS VEGAS -- After his team's 83-63 win over UCLA on Tuesday night at T-Mobile Arena, Gonzaga's Drew Timme said the Bulldogs had not sent a message with their dominant victory to anyone outside their… [+2968 chars]",
    },
    {
      source: {
        id: null,
        name: 'ScienceAlert',
      },
      author: 'Tessa Koumoundouros',
      title:
        "This Ancient Human Relative 'Walked Like a Human, But Climbed Like an Ape' - ScienceAlert",
      description:
        'Sometime between 7-6 million years ago, our primate ancestors stood up and began to walk on two legs.',
      url: 'https://www.sciencealert.com/this-ancient-human-relative-walked-like-a-human-but-climbed-like-an-ape',
      urlToImage:
        'https://www.sciencealert.com/images/2021-11/processed/IssaPortraitAndFullBody_1024.jpg',
      publishedAt: '2021-11-24T06:34:38Z',
      content:
        'Sometime between 7-6 million years ago, our primate ancestors stood up and began to walk on two legs.\r\nA defining moment along the winding evolutionary roads to becoming human, this is the feature re… [+3728 chars]',
    },
    {
      source: {
        id: null,
        name: 'Fox Business',
      },
      author: 'Dom Calicchio',
      title:
        'Biden expected to nominate Shalanda Young for budget director - Fox Business',
      description:
        'President Biden on Wednesday is expected to nominate Shalanda Young to be permanent director of the Office of Management and Budget, Fox News has learned.',
      url: 'https://www.foxbusiness.com/politics/biden-shalanda-young-budget-director-omb-nomination',
      urlToImage:
        'https://a57.foxnews.com/static.foxbusiness.com/foxbusiness.com/content/uploads/2021/11/0/0/Shalanda-Youngggg.jpg?ve=1&tl=1',
      publishedAt: '2021-11-24T06:24:53Z',
      content:
        'President Biden on Wednesday is expected to nominate Shalanda Young to be permanent director of the Office of Management and Budget, Fox News has learned.\r\nIf confirmed by the Senate, Young would bec… [+1704 chars]',
    },
    {
      source: {
        id: null,
        name: 'New York Post',
      },
      author: 'Kenneth Garger',
      title:
        'Kyle Rittenhouse met with Donald Trump at Mar-a-Lago after acquittal - New York Post',
      description:
        'Kyle Rittenhouse met with Donald Trump at the former president’s Mar-a-Lago club after the teen was acquitted of all charges in his homicide trial last week.',
      url: 'https://nypost.com/2021/11/24/kyle-rittenhouse-and-donald-trump-meet-at-mar-a-lago-after-acquittal/',
      urlToImage:
        'https://nypost.com/wp-content/uploads/sites/2/2021/11/trump-rittenhouse-index.jpg?quality=90&strip=all&w=1024',
      publishedAt: '2021-11-24T05:13:00Z',
      content:
        'Kyle Rittenhouse met with Donald Trump at the former presidents Mar-a-Lago club after the teen was acquitted of all charges in his homicide trial last week.\r\nThe meeting between the two was disclosed… [+1396 chars]',
    },
    {
      source: {
        id: 'google-news',
        name: 'Google News',
      },
      author: null,
      title:
        'Amazon Studios Is Closing in on a Mass Effect Adaptation | CBR - CBR',
      description: null,
      url: 'https://news.google.com/__i/rss/rd/articles/CBMiPWh0dHBzOi8vd3d3LmNici5jb20vbWFzcy1lZmZlY3QtYWRhcHRhdGlvbi1kZWFsLWNsb3NlLWFtYXpvbi_SAUFodHRwczovL3d3dy5jYnIuY29tL21hc3MtZWZmZWN0LWFkYXB0YXRpb24tZGVhbC1jbG9zZS1hbWF6b24vYW1wLw?oc=5',
      urlToImage: null,
      publishedAt: '2021-11-24T04:58:00Z',
      content: null,
    },
    {
      source: {
        id: null,
        name: 'YouTube',
      },
      author: null,
      title:
        "India: 'Cryptocurrency regulation' regulation bill could be tabled soon | WION | World English News - WION",
      description:
        'The India Prime Minister Narendra Modi-led government has listed a bill for the upcoming Parliament session that reportedly seeks to prohibit all private cry...',
      url: 'https://www.youtube.com/watch?v=QXfxbBsdJck',
      urlToImage: 'https://i.ytimg.com/vi/QXfxbBsdJck/maxresdefault.jpg',
      publishedAt: '2021-11-24T04:39:25Z',
      content: null,
    },
    {
      source: {
        id: null,
        name: 'BBC News',
      },
      author: 'https://www.facebook.com/bbcnews',
      title:
        'US jury awards $25m in damages over Unite the Right rally - BBC News',
      description:
        'Those injured in Charlottesville accused organisers of conspiring to commit racially motivated violence.',
      url: 'https://www.bbc.com/news/world-us-canada-59355019',
      urlToImage:
        'https://ichef.bbci.co.uk/news/1024/branded_news/0C4B/production/_121774130_rally.jpg',
      publishedAt: '2021-11-24T04:36:18Z',
      content:
        'Image source, Getty Images\r\nImage caption, Richard Spencer, who coined the term "alt-right", was among the defendants\r\nA US jury has awarded $25m (£19m) in damages against the organisers of a deadly … [+4365 chars]',
    },
    {
      source: {
        id: null,
        name: 'WSB Atlanta',
      },
      author: 'Theresa Seiger, Cox Media Group National Content Desk',
      title:
        'Brian Laundrie died of self-inflicted gunshot wound, medical examiner confirms - WSB Atlanta',
      description:
        'An attorney representing the family of Brian Laundrie told reporters Tuesday that the 23-year-old died of a self-inflicted gunshot wound.',
      url: 'https://www.wsbtv.com/news/trending/brian-laundrie-autopsy-update-expected-soon-attorney-says/VZI3FVMZFRHGPLOHZSULCXS2XE/',
      urlToImage:
        'https://cmg-cmg-tv-10010-prod.cdn.arcpublishing.com/resizer/w_92Kq2xvb0mHw_MOw5bDc4Cf3s=/1440x810/filters:format(jpg):quality(70)/cloudfront-us-east-1.images.arcpublishing.com/cmg/T7STTZNOG5CY7EZOQYTESKAK4E.jpg',
      publishedAt: '2021-11-24T04:34:00Z',
      content:
        'An attorney representing the family of Brian Laundrie told reporters Tuesday that the 23-year-old died of a self-inflicted gunshot wound.\r\n&gt;&gt; Read more trending news\r\nAuthorities found Laundrie… [+3086 chars]',
    },
  ],
};
